# JavaScript plugin list must have in website
## Dialogs
##### [Alertify.js]
  * [URL](http://fabien-d.github.com/alertify.js/)/[Github](https://github.com/fabien-d/alertify.js)

##### [Vex]
  * [URL](http://github.hubspot.com/vex/docs/welcome/)/[Github](https://github.com/hubspot/vex)

##### [Avgrund Modal]
  * [URL](http://labs.voronianski.com/jquery.avgrund.js/)/[Github](https://github.com/voronianski/jquery.avgrund.js)

## AnimationCSS3
##### [Animate.css]
  * [URL](https://daneden.github.io/animate.css/)/[Github](https://github.com/daneden/animate.css)


## Forms
##### [icheck]
  * [URL](http://icheck.fronteed.com/)/[Github](https://github.com/daneden/animate.css)

##### [Bootstrap custom checkbox]
  * [URL](https://github.com/flatlogic/awesome-bootstrap-checkbox)

##### [long-press]
  * [URL](http://toki-woki.net/lab/long-press/)/[Github](https://github.com/quentint/long-press)

##### [jQuery Knob]
  * [URL](http://anthonyterrien.com/demo/knob/)/[Github](https://github.com/aterrien/jQuery-Knob)

##### [Fancy input]
  * [URL](http://yaireo.github.io/fancyInput/)/[Github](https://github.com/yairEO/fancyInput/)

##### [Type ahead]
  * [URL](http://twitter.github.io/typeahead.js/)/[Github](https://github.com/twitter/typeahead.js/)

##### [Type ahead]
  * [URL](http://twitter.github.io/typeahead.js/)/[Github](https://github.com/twitter/typeahead.js/)

##### [Parsley validator]
  * [URL](http://parsleyjs.org/doc/examples/simple.html)/[Github](https://github.com/guillaumepotier/Parsley.js/)

##### [Windows]
  * [URL](http://nick-jonas.github.io/windows/)/[Github](https://github.com/nick-jonas/nick-jonas.github.com/tree/master/windows)

##### [ScrollToFixed]
  * [URL](https://bigspotteddog.github.io/ScrollToFixed/)/[Github](https://github.com/bigspotteddog/ScrollToFixed)

##### [textillate.js]
  * [URL](http://textillate.js.org/)/[Github](https://github.com/jschr/textillate/)

##### [fittextjs]
  * [URL](http://fittextjs.com/)/[Github](https://github.com/davatron5000/FitText.js)

##### [jquery-backstretch]
  * [URL](http://www.jquery-backstretch.com)/[Github](https://github.com/jquery-backstretch/jquery-backstretch)


